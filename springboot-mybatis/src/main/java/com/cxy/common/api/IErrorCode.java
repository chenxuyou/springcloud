package com.cxy.common.api;


public interface IErrorCode {
    long getCode();

    String getMessage();
}
