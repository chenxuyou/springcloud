package com.cxy.controller;

import com.cxy.common.api.CommonPage;
import com.cxy.common.api.CommonResult;
import com.cxy.model.PmsBrandDo;
import com.cxy.service.PmsBrandDoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.Semaphore;

@Controller
@RequestMapping("/brand")
public class PmsBrandDoController {
    @Autowired
    private PmsBrandDoService brandService;

    private Semaphore semaphore =new Semaphore(1);

    private static final Logger LOGGER = LoggerFactory.getLogger(PmsBrandDoController.class);

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult createBrand(@RequestBody PmsBrandDo PmsBrandDo) {
        CommonResult commonResult;
        int count = brandService.createBrand(PmsBrandDo);
        if (count == 1) {
            commonResult = CommonResult.success(PmsBrandDo);
            LOGGER.debug("createBrand success:{}", PmsBrandDo);
        } else {
            commonResult = CommonResult.failed("操作失败");
            LOGGER.debug("createBrand failed:{}", PmsBrandDo);
        }
        return commonResult;
    }

   
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult updateBrand(@PathVariable("id") Long id, @RequestBody PmsBrandDo PmsBrandDoDto, BindingResult result) {
        CommonResult commonResult;
        int count = brandService.updateBrand(id, PmsBrandDoDto);
        if (count == 1) {
            commonResult = CommonResult.success(PmsBrandDoDto);
            LOGGER.debug("updateBrand success:{}", PmsBrandDoDto);
        } else {
            commonResult = CommonResult.failed("操作失败");
            LOGGER.debug("updateBrand failed:{}", PmsBrandDoDto);
        }
        return commonResult;
    }

  
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult deleteBrand(@PathVariable("id") Long id) {
        int count = brandService.deleteBrand(id);
        if (count == 1) {
            LOGGER.debug("deleteBrand success :id={}", id);
            return CommonResult.success(null);
        } else {
            LOGGER.debug("deleteBrand failed :id={}", id);
            return CommonResult.failed("操作失败");
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult<PmsBrandDo> brand(@PathVariable("id") Long id) {
        PmsBrandDo brand =new PmsBrandDo();
        int availablePermits=semaphore.availablePermits();
        if(availablePermits>0){
            System.out.println("抢到资源");
        }else{
            System.out.println("资源已被占用，稍后再试");
            return CommonResult.failed("资源已被占用，稍后再试");
        }

        try {
            semaphore.acquire(1);
            brand=  brandService.getBrand(id);

        } catch (InterruptedException e) {

            e.printStackTrace();
        }finally {
            semaphore.release();
        }
        return CommonResult.success(brand);
    }
}
