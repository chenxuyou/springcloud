/*
package com.cxy.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.aspectj.weaver.ast.Var;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class Start {
    NioEventLoopGroup boss= new NioEventLoopGroup();
    NioEventLoopGroup work= new NioEventLoopGroup();

    @PostConstruct
    public void start() throws InterruptedException{
        ServerBootstrap bootstrap =new ServerBootstrap();
        bootstrap.group(boss,work)
                .channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG,1024)
                .childOption(ChannelOption.SO_KEEPALIVE,true)
                .handler(new LoggingHandler(LogLevel.INFO))
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    public void initChannel(SocketChannel sc) throws Exception {
                        sc.pipeline().addLast(new ServerUAVHandler());
                    }
    }
}
*/
