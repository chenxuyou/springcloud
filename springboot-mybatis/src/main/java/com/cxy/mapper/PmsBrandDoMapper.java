package com.cxy.mapper;

import com.cxy.model.PmsBrandDo;

public interface PmsBrandDoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(PmsBrandDo record);

    int insertSelective(PmsBrandDo record);

    PmsBrandDo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PmsBrandDo record);

    int updateByPrimaryKeyWithBLOBs(PmsBrandDo record);

    int updateByPrimaryKey(PmsBrandDo record);
}