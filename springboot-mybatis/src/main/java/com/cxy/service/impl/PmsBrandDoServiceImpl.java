package com.cxy.service.impl;

import com.cxy.mapper.PmsBrandDoMapper;
import com.cxy.model.PmsBrandDo;
import com.cxy.service.PmsBrandDoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PmsBrandDoServiceImpl  implements PmsBrandDoService {

    @Autowired(required = false)
    private PmsBrandDoMapper pmsBrandDoMapper;


    @Override
    public int createBrand(PmsBrandDo brand) {
        return pmsBrandDoMapper.insertSelective(brand);
    }

    @Override
    public int updateBrand(Long id, PmsBrandDo brand) {
        brand.setId(id);
        return pmsBrandDoMapper.updateByPrimaryKeySelective(brand);
    }

    @Override
    public int deleteBrand(Long id) {
        return pmsBrandDoMapper.deleteByPrimaryKey(id);
    }


    @Override
    public PmsBrandDo getBrand(Long id) {
        return pmsBrandDoMapper.selectByPrimaryKey(id);
    }
}
