package com.cxy.service;

import com.cxy.model.PmsBrandDo;

import java.util.List;

public interface PmsBrandDoService {

    int createBrand(PmsBrandDo brand);

    int updateBrand(Long id, PmsBrandDo brand);

    int deleteBrand(Long id);


    PmsBrandDo getBrand(Long id);
}
